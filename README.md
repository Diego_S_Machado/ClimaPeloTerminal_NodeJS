<h1>Clima no terminal</h1>
Autor: Andrew Mead, Rob Percival.
Esta é uma aula da Udemy que estou seguindo o passo a passo.

Aula sobre Webscraping com NodeJS

Para usar o programa deve executar
<code>
  node app.js -a "Endereço que deseja saber o clima"
</code>

Caso esteja com dúvida, use o help
<code>
  node app.js -h
</code>
